//STATION DEFENDER V.08
//https://gitlab.com/aja1/stationdefender
//University Project for Mondragon University


/*==================================[MAIN]=================================*/
function main() {
    /*==================================[VARIABLES]=================================*/
    //Indicamos que vamos a trabajar con Canvas, basandonos en gameArea
    var canvas = document.getElementById("canvasArea");
    var ctx = canvas.getContext("2d"); // contexto para dibujar en Canvas

    //xB e yB indican la posicion inicial de la pelota (b de Ball)
    var canvasFifth = (canvas.width / 2) - (canvas.width / 2) / 5;

    var xB = canvas.width / 2 + genRandomNumber(-canvasFifth, canvasFifth);
    // var xB = canvas.width / 2; //la mitad del canvas
    var yB = canvas.height - 30; //damos un poco de margen

    //variables para resetear la pelota
    var startPosX = xB;
    var startPosY = yB;

    //variables de movimiento de la pelota
    if (xB > canvas.width / 2) {
        var xM = -1;
    } else {
        var xM = 1;
    }

    var yM = -1;

    var points = 0; //puntos
    var mult = 1; //multiplicador de puntos

    var ballHitbox = 10; //tamano del radio de la pelota

    var playerHeight = 15; //altura del jugador
    var playerWidth = 75; //longitud del jugador
    var playerX = (canvas.width - playerWidth) / 2; // centramos el jugador
    var playerColor = "#3264a8";
    var playerLives = 3; // vidas del jugador

    //Valores de los enemigos
    var enemyRows = 4;
    var enemyColumns = 7;
    var enemyWidth = 55;
    var enemyHeight = 20;
    var enemyPadding = 3;
    var enemyOffsetTop = 30; //esto los usamos para mover los enemigos lejos de los bordes de la pantalla
    var enemyOffsetLeft = 35;

    // Valores de el disparo
    var shootVariable = 0;
    var shotNumber = 3; // esto va a ser el numero de disparos que tendrá el jugador
    var shotHitbox = 5;
    var shotY = 320;
    var shotX = playerX;

    //movimientos
    //son variables globales pero pueden cambiar
    var rightPressed = false;
    var leftPressed = false;
    var upPressed = false;

    /*==================================[SONIDOS]=================================*/
    var bounce;
    bounce = new sound("sfx/bounce.wav"); //meter esto en el main
    var gameOverSound;
    gameOverSound = new sound("sfx/gameOver.wav");
    var enemyDeadSound;
    enemyDeadSound = new sound("sfx/enemyDead.wav");
    var winSound;
    winSound = new sound("sfx/win.wav");

    //posicion del raton
    function getMousePos(canvas, event) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
        };
    }

    function menu() {
        /*==================================[MENU]=================================*/
        //variables tamano de botones
        var buttonWidth = 250;
        var buttonHeight = 50;
        var buttonCenterX = (canvas.width - buttonWidth) / 2;

        function drawEasy() {
            //boton
            ctx.beginPath();
            ctx.rect(buttonCenterX, 40, buttonWidth, buttonHeight); // dibujamos el rectangulo, con sus posiciones y tamanos
            ctx.fillStyle = "#0dbf10";
            ctx.fill();
            ctx.closePath();
            //texto
            ctx.font = "30px Courier New";
            ctx.fillStyle = "#ffffff";
            ctx.textAlign = "center"; //centramos el texto
            ctx.fillText("E A S Y", 240, 75);
        }
        function drawNormal() {
            //boton
            ctx.beginPath();
            ctx.rect(buttonCenterX, 100, buttonWidth, buttonHeight); // dibujamos el rectangulo, con sus posiciones y tamanos
            ctx.fillStyle = "#3264a8";
            ctx.fill();
            ctx.closePath();
            //texto
            ctx.font = "30px Courier New";
            ctx.fillStyle = "#ffffff";
            ctx.textAlign = "center";
            ctx.fillText("M E D I U M", 240, 135);
        }
        function drawHard() {
            //boton
            ctx.beginPath();
            ctx.rect(buttonCenterX, 160, buttonWidth, buttonHeight); // dibujamos el rectangulo, con sus posiciones y tamanos
            ctx.fillStyle = "#9c0202";
            ctx.fill();
            ctx.closePath();
            //texto
            ctx.font = "30px Courier New";
            ctx.fillStyle = "#ffffff";
            ctx.textAlign = "center";
            ctx.fillText("H A R D", 240, 195);
        }
        //Dibujamos el menu
        drawEasy();
        drawNormal();
        drawHard();

        /*==================================[LISTENER Y CHECK DE MENU]=================================*/

        //buf, a ver:
        //si la posicion del raton esta en las coordenadas que se crea el rectangulo de EASY
        //o dentro de su altura y longitud
        //se le pasara una posicion, que la conseguimos con un eventlistener
        //hay que estandarirazlo, pasar un offset en vez de coordenadas hardcodeadas?

        function isInsideEasy(pos) {
            return pos.x > buttonCenterX && pos.x < buttonCenterX + buttonWidth && pos.y < 40 + buttonHeight && pos.y > 40;
        }
        function isInsideMedium(pos) {
            return pos.x > buttonCenterX && pos.x < buttonCenterX + buttonWidth && pos.y < 100 + buttonHeight && pos.y > 100;
        }
        function isInsideHard(pos) {
            return pos.x > buttonCenterX && pos.x < buttonCenterX + buttonWidth && pos.y < 160 + buttonHeight && pos.y > 160;
        }

        function clickHandler(e) {
            var mousePos = getMousePos(canvas, e);

            //igual un case? no se, 3 condiciones igual no merecen la pena, complica el codigo
            if (isInsideEasy(mousePos)) {

                playerWidth += 20; //ampliamos el tamano del jugador
                playerColor = "#0dbf10"; //cambiamos el color al facil
                playerLives = 3;
				shotNumber = 3;
                gameStart();
                canvas.removeEventListener('click', clickHandler, false); //en cuanto el menu cumpla su funcion quitamos el listener

            } else if (isInsideMedium(mousePos)) {

                gameStart();
                playerLives = 2;
				shotNumber = 1;
                //no hace falta cambiar color
                canvas.removeEventListener('click', clickHandler, false); //en cuanto el menu cumpla su funcion quitamos el listener

            } else if (isInsideHard(mousePos)) {

                playerWidth -= 20; //hacemos que el jugador sea mas pequeno
                playerColor = "#9c0202"; //cambiamos el color al dificil
                playerLives = 0;
				shotNumber = 0;
                gameStart();
                canvas.removeEventListener('click', clickHandler, false); //en cuanto el menu cumpla su funcion quitamos el listener

            }
        }
        canvas.addEventListener('click', clickHandler, false);
        //}
    }
    menu();

    function gameStart() {

        /*==================================[DIBUJAR PUNTOS EN PANTALLA]=================================*/
        function drawPoints() {
            ctx.font = "15px Sans serif";
            ctx.fillStyle = "#ffffff";
            ctx.fillText("Puntuak: " + points, 70, 25);
        }

        function drawLives() {
            ctx.font = "15px Sans serif";
            ctx.fillStyle = "#ffffff";
            ctx.fillText("Lives: " + playerLives, canvas.width / 2, 25);
        }
		function drawShots() {
            ctx.font = "15px Sans serif";
            ctx.fillStyle = "#ffffff";
            ctx.fillText("Shots left: " + shotNumber, canvas.width / 2, canvas.height - playerHeight - 3);
        }
        function drawMult() {
            ctx.font = "15px Sans serif";

            //diferentes colores para diferentes combos! :)
            switch (true) {
            case (mult < 5):
                ctx.fillStyle = "#ffffff";
                break;
            case (mult >= 5 && mult < 10):
                ctx.fillStyle = "#f1ff26";
                break;
            case (mult >= 10 && mult < 20):
                ctx.fillStyle = "#ff9d26";
                break;
            case (mult >= 20):
                ctx.fillStyle = "#ff0000";
                break;
            }

            ctx.fillText("Konbo: x" + mult, canvas.width - 75, 25);
        }

        /*==================================[CREACION ENEMIGOS]=================================*/

        //i= columnas
        //j= filas
        //el for recorre cuantas columnas tenemos para saber cuantas veces hay que hacer las filas

        var enemyArray = []; //array donde guardaremos los datos de los enemigos

        for (var i = 0; i < enemyColumns; i++) {

            enemyArray[i] = []; //creamos un array bidimensional (dos dimensiones)
            //ahora es un array bidimensional, i sera el array de las columnas e y será el de las filas

            for (var j = 0; j < enemyRows; j++) {
                //por cada posicion de fila creamos un enemigo que se anadira al array
                //los enemigos que se crean tienen 2 coordenadas, se ponen en un array juntos
                //es decir, cada columna tendra su fila de objetos
                //un array de dos dimensiones puede guardar dos valores diferentes en una posicion, tendran un nombre cada uno
                //en este caso sera x e y del enemigo
                enemyArray[i][j] = {
                    x: 0,
                    y: 0,
                    hit: 0
                }; // a ese objeto le asignamos coordenadas x e y , iniciadas a 0 ( de la columna 0, la fila 0, columna 0, fila 2, etc)

            }
        }

        /*==================================[DIBUJO DE ENEMIGOS]=================================*/
        function drawEnemies() {
            for (var i = 0; i < enemyColumns; i++) { //para las columnas
                for (var j = 0; j < enemyRows; j++) { //para las filas
                    //si NO se le ha dado al rectangulo, hacemos esto
                    if (enemyArray[i][j].hit != 1) {
                        //i columnas, j num filas. Con esto multiplicamos la fila en la que esta por su anchura y su padding ( espacio entre enemigos)
                        //y luego le metemos el offset para que no aparezca en las esquinas
                        var enemyX = (i * (enemyWidth + enemyPadding)) + enemyOffsetLeft;
                        var enemyY = (j * (enemyHeight + enemyPadding)) + enemyOffsetTop;

                        //en el array metemos los dos valores de coordenadas que hemos calculado,
                        enemyArray[i][j].x = enemyX;
                        enemyArray[i][j].y = enemyY;

                        ctx.beginPath();
                        ctx.rect(enemyX, enemyY, enemyWidth, enemyHeight); //en las coordenadas calculadas dibujamos un rectangulo con propiedades que pasamos
                        ctx.fillStyle = "#e34240";
                        ctx.fill();
                        ctx.closePath();
                    }

                }
            }
        }
        /*==================================[COLISIONES ENEMIGO]=================================*/
        function collissionEnemy() {
            for (var i = 0; i < enemyColumns; i++) { //para las columnas
                for (var j = 0; j < enemyRows; j++) {
                    var en = enemyArray[i][j]; //creamos una variable por cada enemigo, con su x e y

                    //si la pelota todavia no se la ha dado comprobamos colisiones
                    if (en.hit != 1) {
                        //si la x de la pelota es menor que la posicion + longitud del enemigo Y
                        //la y de la pelota es mayor que la posicion y + su altura, rebota
                        //if (yB > en.y && yB < en.y + enemyHeight && xB > en.x && xB < en.x + enemyWidth) {
                        if (yB + ballHitbox > en.y && yB - ballHitbox < en.y + enemyHeight && xB > en.x && xB < en.x + enemyWidth) {
                            enemyDeadSound.stop();
                            enemyDeadSound.play();
                            //por cada enemigo damos 5 puntos e incrementamos el multiplicador
                            points += 5 * mult;
                            mult++;
                            en.hit = 1; //marcamos como que le ha dado
                            yM = -yM;
                            if (mult == enemyRows * enemyColumns + 1) {
                                enemyDeadSound.stop();
                                winSound.play();
                                alert("!!!Y O U  W I N!!!\nPuntu totalak : " + points);
                                document.location.reload(); // recargamos el juego
                                clearInterval(interval); // se necesita limpiar el intervalo para poder usarlo de nuevo en chrome
                            }
                        }
                        if (shotY > en.y && shotY < en.y + enemyHeight && shotX > en.x && shotX < en.x + enemyWidth) {
                            enemyDeadSound.stop();
                            enemyDeadSound.play();
                            points = points - 250;
                            mult++;
                            en.hit = 1;
                            shootVariable = 0;
                            shotNumber--;
                            if (shootVariable == 0) {
                                shotX = playerX;
                                shotY = 320;
                            }
                        }
                    }
                }
            }
        }
        /*==================================[ FUN. DIBUJAR DISPARO ]=========================================*/
        function drawShoot() {
            if (shootVariable == 1) {
				/*
                ctx.beginPath();
                ctx.arc(shotX, shotY, shotHitbox, 0, Math.PI * 2); // dibujamos el disparo
                ctx.fillStyle = "#f5da42"; // color del disparo
                ctx.fill(); //rellenamos el disparo
                ctx.closePath(); // terminamos de dibujar*/
            }

            if (shotY <= 0) {
                shotX = playerX;
                shotY = 320;
                shootVariable = 0;
                shotNumber--;
            }
        }

        /*==================================[ FUN. DIBUJAR JUGADOR Y PELOTA]=================================*/
        function drawBall() {
            ctx.beginPath(); //empezamos a dibujar
            ctx.arc(xB, yB, ballHitbox, 0, Math.PI * 2); //dibujamos un circulo, primeras dos son la pos origen
            ctx.fillStyle = "#4dfff9"; //color del ciruclo
            ctx.fill(); //rellenamos la forma
            ctx.closePath(); //terminamos de dibujar
        }

        function drawPlayer() {
            ctx.beginPath();
            ctx.rect(playerX, canvas.height - playerHeight, playerWidth, playerWidth); // dibujamos el rectangulo, con sus posiciones y tamanos
            ctx.fillStyle = playerColor;
            ctx.fill();
            ctx.closePath();
        }

        /*==================================[FUNCION UPDATE, DIBUJAR Y ACTUALIZAR]=================================*/
        function updateGame() {

            ctx.clearRect(0, 0, canvas.width, canvas.height); //funcion para limpiar desde 0,0 hasta el final del canvas

            drawEnemies();
            drawBall();
            drawPlayer();
            collissionEnemy();
            drawPoints();
			drawShots()
            drawMult();
            drawLives();
			
			
            //Si las coordenadas de la pelota ( y su siguiente movimiento) son superiores al tamano del canvas, invertimos la direccion de movimiento
            //usamos el diametro del circulo para ver si entra la pelota en el canvas en el siguiente frame
            if (xB + xM > canvas.width - ballHitbox || xB + xM < ballHitbox) {
                xM = -xM; // invertimos el movimiento horizontal
                bounce.stop();
                bounce.play();
            }

            if (yB + yM < ballHitbox) {
                yM = -yM; // invertimos el movimiento vertical
                bounce.stop();
                bounce.play();

            } else if (xB > playerX && xB < playerX + playerWidth && yB + ballHitbox > canvas.height - playerHeight) { //en caso de que la posicion de la pelota sea mayor que la x del jugador o su longitud, rebotamos

                yM = -yM;

                if (yM > -10 && xM > -10) { //10 sera el maximo de velocidad que tendra la pelota
                    yM--;
                    xM--;
                }
                bounce.stop();
                bounce.play();
                points = points + 1 * mult; //por cada bote 1 punto por el multiplicador

            } else if (yB + ballHitbox > canvas.height) { //impacto con la zona de abajo, ya que sabemos que su altitud menos el hitbox da el final
                // si no esta entre la longitud del jugador
                if (playerLives == 0) {
                    bounce.stop();
                    gameOverSound.stop();
                    gameOverSound.play();
                    alert("GAME OVER\nPuntuak: " + points);
                    document.location.reload(); // recargamos el juego
                    clearInterval(interval); // se necesita limpiar el intervalo para poder usarlo de nuevo en chrome
                } else {
                    playerLives--;

                    alert("You lost a life!. Lives left: " + playerLives);
                    xB = startPosX;
                    yB = startPosY;
                    if (xB > canvas.width / 2) {
                        xM = -1;
                    } else {
                        xM = 1;
                    }

                    yM = -1;
                }

            }

            //checks de movimiento, si es verdadero, lo movemos
            if (rightPressed) {
                playerX += 6;
                //para que no se salga del canvas
                if (playerX + playerWidth > canvas.width) {
                    playerX = canvas.width - playerWidth;
                }
            } else if (leftPressed) {
                playerX -= 6;
                //para que no se salga del canvas
                if (playerX < 0) {
                    playerX = 0;
                }
            }

            //-------- lógica del disparo --------
            if ((upPressed) && (shotNumber > 0)) {
                shootVariable = 1;
            }

            if (shootVariable > 0) {
                shotY = shotY - 5;
                ctx.beginPath();
                ctx.arc(shotX, shotY, shotHitbox, 0, Math.PI * 2); // dibujamos el disparo
                ctx.fillStyle = "#f5da42"; // color del disparo
                ctx.fill(); //rellenamos el disparo
                ctx.closePath();
            }
			
			
            // si el jugador no le da a la tecla del disparo la posicion del jugador se le pasara al valor x del disparo
            if (shootVariable == 0) {
                shotX = playerX + 75 / 2;
            }
            //*Ahora para hacer las fisicas del disparo tenemos que fijarnos en el eje x si hay alguien más en el eje x el objeto se borraro junto al disparo cuando esten en la misma posicion en el eje y


            xB += xM;
            yB += yM;

        }
        /*==================================[MOVIMIENTO]=================================*/
        //dos listeners. Escuchan a ver si las teclas estan siendo pulsadas
        document.addEventListener("keydown", keyDownHandler, false);
        document.addEventListener("keyup", keyUpHandler, false);

        //ArrowRight es la flecha derecha
        //Se usa Right a secas tambien para dar soporte a navegadores antigues, IE

        function keyDownHandler(e) {
            if (e.key == "Right" || e.key == "ArrowRight") {
                rightPressed = true; //cambiamos la variables a verdad si se esta manteniendo
            } else if (e.key == "Up" || e.key == "ArrowUp") {
                upPressed = true;
            } else if (e.key == "Left" || e.key == "ArrowLeft") {
                leftPressed = true;
            }

        }
        function keyUpHandler(e) {
            if (e.key == "Right" || e.key == "ArrowRight") {
                rightPressed = false;
            } else if (e.key == "Up" || e.key == "ArrowUp") {
                upPressed = false;
            } else if (e.key == "Left" || e.key == "ArrowLeft") {
                leftPressed = false;
            }
        }

        //meter en bucle
        var interval = setInterval(updateGame, 20); // el intervalo de cuantas veces se van a dibujar los items, 20 frames en este caso, lo ponemos como variable para chrome

    } //endgamestart


    /*==================================[CONSTRUCTOR SONIDOS]=================================*/
    function sound(src) {
        this.sound = document.createElement("audio"); //tipo de elemento que vamos a crear
        this.sound.src = src; //origen (archivo) del sonido
        this.sound.setAttribute("preload", "auto");
        this.sound.setAttribute("controls", "none"); //no tendra ningun control, objeto a secas
        this.sound.style.display = "none"; //no se vera
        document.body.appendChild(this.sound); //el sonido estara dentro del body
        this.play = function () { //funcion que hace que suene el sonido
            this.sound.play();
        }
        this.stop = function () { //funcion que para el sonido
            this.sound.pause();
        }
    }

} //endmain

function genRandomNumber(min, max) {
    return Math.random() * (max - min) + min; // convertimos el floating popint de 0-1 a un minimo y con un maximo
}
